package model;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Module {
    private String code;
    private String name;

    private List<Person> students = new ArrayList<Person>();

    public Module() {
    }

    public Module(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public int size() {
        return students.size();
    }

    public boolean add(Person person) {
        return students.add(person);
    }

    public boolean remove(Object o) {
        return students.remove(o);
    }

    public Person set(int index, Person element) {
        return students.set(index, element);
    }

    public Person get(int index) {
        return students.get(index);
    }

    public List<Person> getStudents() {
        return students;
    }

    public void forEach(Consumer<? super Person> action) {
        students.forEach(action);
    }
}
