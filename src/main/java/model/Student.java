package model;

public class Student extends Person {
    long studyId;

    public Student() {
        studyId = -1L;
    }

    public Student(String firstname, String lastname, long studyId) {
        super(firstname, lastname);
        this.studyId = studyId;
    }

    public long getStudyId() {
        return studyId;
    }

    public void setStudyId(long studyId) {
        this.studyId = studyId;
    }

    @Override
    public String toString() {
        return "Student{firstname='" + getFirstname() +
                "', lastname='" + getLastname() +
                "',studyId=" + studyId +
                '}';
    }
}
