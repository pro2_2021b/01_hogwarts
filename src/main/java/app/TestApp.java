package app;

import model.Module;
import model.Person;
import model.Student;

public class TestApp {
    public static void main(String[] args) {
        Module pro2 = new Module("PRO2", "Programovani II");

        pro2.add(new Person("Petr", "Novak"));
        pro2.add(new Person("Jan", "Zastava"));
        pro2.add(new Student("Petr", "Student",12345));
        pro2.add(new Student("Andrea", "Kytkova",54321));
        pro2.add(new Person("Jan", "Zastava"));
        for (int i = 0; i < pro2.size(); i++) {
            System.out.println(pro2.get(i));
        }
        System.out.println("---------------VYPIS POMOCI LAMBDY");
        pro2.forEach(student -> System.out.println(student));
    }
}
